#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.highlight 3.18.90\n"
"PO-Revision-Date: 2023-12-21 11:03 UTC\n"
"Last-Translator: benoit <benoit@benoit-kubuntu>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: TextHighlighterTheme.class:78
msgid "Added"
msgstr "Ajout"

#: TextHighlighterTheme.class:78
msgid "At rule"
msgstr "Règle \"@\""

#: TextHighlighterTheme.class:78
msgid "Attribute"
msgstr "Attribut"

#: TextHighlighterTheme.class:78
msgid "Background"
msgstr "Arrière-plan"

#: TextHighlighterTheme.class:78
msgid "Breakpoint"
msgstr "Point d'arrêt"

#: TextHighlighter.class:67
msgid "C"
msgstr "C"

#: TextHighlighter.class:69
msgid "C++"
msgstr "C++"

#: TextHighlighter.class:71
msgid "Cascading Style Sheet"
msgstr "Feuille de style"

#: TextHighlighterTheme.class:78
msgid "Class"
msgstr "Classe"

#: TextHighlighterTheme.class:78
msgid "Color"
msgstr "Couleur"

#: TextHighlighterTheme.class:78
msgid "Command"
msgstr "Commande"

#: TextHighlighterTheme.class:78
msgid "Comment"
msgstr "Commentaire"

#: TextHighlighterTheme.class:78
msgid "Constant"
msgstr "Constante"

#: TextHighlighterTheme.class:78
msgid "Current line"
msgstr "Ligne courante"

#: TextHighlighterTheme.class:78
msgid "Datatype"
msgstr "Type de données"

#: TextHighlighter.class:73 TextHighlighterTheme.class:78
msgid "Diff"
msgstr "Différences"

#: TextHighlighterTheme.class:78
msgid "Documentation"
msgstr "Documentation"

#: TextHighlighterTheme.class:78
msgid "Entity"
msgstr "Entité"

#: TextHighlighterTheme.class:78
msgid "Error"
msgstr "Erreur"

#: TextHighlighterTheme.class:78
msgid "Escape"
msgstr "Échappement"

#: TextHighlighterTheme.class:78
msgid "Expansion"
msgstr "Expansion"

#: TextHighlighterTheme.class:78
msgid "File"
msgstr "Fichier"

#: TextHighlighterTheme.class:78
msgid "Function"
msgstr "Fonction"

#: TextHighlighter.class:92
msgid "Gambas"
msgstr "Gambas"

#: TextHighlighter.class:83
msgid "Gambas Web Page"
msgstr "Page web Gambas"

#: TextHighlighterTheme.class:78
msgid "Highlight"
msgstr "Mise en évidence"

#: TextHighlighter.class:75
msgid "HTML"
msgstr "HTML"

#: TextHighlighterTheme.class:78
msgid "Id"
msgstr "Identifiant"

#: TextHighlighterTheme.class:78
msgid "Identifier"
msgstr "Identificateur"

#: TextHighlighterTheme.class:78
msgid "Important"
msgstr "Important"

#: TextHighlighterTheme.class:78
msgid "Index"
msgstr "Index"

#: TextHighlighter.class:77
msgid "Javascript"
msgstr "Javascript"

#: TextHighlighterTheme.class:78
msgid "Keyword"
msgstr "Mot-clef"

#: TextHighlighterTheme.class:78
msgid "Label"
msgstr "Étiquette"

#: TextHighlighterTheme.class:78
msgid "Markup"
msgstr "Balise"

#: TextHighlighterTheme.class:78
msgid "Normal"
msgstr "Normal"

#: TextHighlighterTheme.class:78
msgid "Number"
msgstr "Nombre"

#: TextHighlighterTheme.class:78
msgid "Operator"
msgstr "Opérateur"

#: TextHighlighterTheme.class:78
msgid "Position"
msgstr "Position"

#: TextHighlighterTheme.class:78
msgid "Preprocessor"
msgstr "Préprocesseur"

#: TextHighlighterTheme.class:78
msgid "Pseudo-class"
msgstr "Pseudo-classe"

#: TextHighlighterTheme.class:78
msgid "Regular expression"
msgstr "Expression rationnelle"

#: TextHighlighterTheme.class:78
msgid "Removed"
msgstr "Suppression"

#: TextHighlighterTheme.class:78
msgid "Selection"
msgstr "Sélection"

#: TextHighlighterTheme.class:78
msgid "Shebang"
msgstr "Interpréteur du script"

#: TextHighlighter.class:79
msgid "Shell"
msgstr "Shell"

#: TextHighlighter.class:81
msgid "SQL"
msgstr "SQL"

#: TextHighlighterTheme.class:78
msgid "String"
msgstr "Chaîne de caractères"

#: TextHighlighterTheme.class:78
msgid "Symbol"
msgstr "Symbole"

#: .project:2
msgid "Syntax highlighter based on definition files."
msgstr "Coloration syntaxique basée sur des fichiers de définition."

#: TextHighlighter.class:85
msgid "Syntax highlighting definition"
msgstr "Définition de coloration syntaxique"

#: TextHighlighterTheme.class:78
msgid "Tag"
msgstr "Tag"

#: TextHighlighterTheme.class:78
msgid "Tag attribute"
msgstr "Attribut de tag"

#: TextHighlighterTheme.class:78
msgid "Unit"
msgstr "Unité"

#: TextHighlighterTheme.class:78
msgid "Value"
msgstr "Valeur"

